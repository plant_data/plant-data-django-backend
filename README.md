## Plants Image Data Collection
The objective of this application is to provide the complete backend functionality for Android/iOS apps required for the collection of training data required for **"Classification of Plant Diseases based on Images"**

### Scope
This application has three kinds of users
1. Scouts: People using the Android/iOS app to upload data to the application backend.
2. Data Validators: People who validate the data entered by the scouts and enter their own data.
3. Website Admins: People who provide access to other users and modify the master data.


* #### Admin Module:
> * Provide interface for admin users to have the complete access over all the other users.
> * Data Operators to view and validate the data entered by the scouts.
> * Provide interfaces to modify the master crops data.

* #### REST APIs for iOS/Android apps:
> * To obtain Auth Token to use in each request's authentication header.
> * To get the user info of the logged in user.
> * To view all available crops in the database as a list.
> * To view details of a particular crop.
> * To view all available diseases.
> * To upload disease image and info needed. (If the disease doesn't exist we need to add to the database for that crop)
> * To review the disease data.
> * To view the all the uploads of a scout.
> * To view all the records a validator has to approve.
> * To validate a particular record.