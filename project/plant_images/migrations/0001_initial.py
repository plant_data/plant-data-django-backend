# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Crop',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('name', models.CharField(max_length=100)),
                ('crop_type', models.CharField(max_length=50, null=True, blank=True)),
                ('additional_comments', models.TextField(default=b'', blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Disease',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('name', models.CharField(max_length=100)),
                ('additional_comments', models.TextField(default=b'', blank=True)),
                ('crop', models.ForeignKey(related_name='diseases', to='plant_images.Crop')),
            ],
        ),
        migrations.CreateModel(
            name='Record',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('picture', models.ImageField(null=True, upload_to=b'disease_pictures', blank=True)),
                ('is_reviewed', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='Reviewer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('additional_comments', models.TextField(default=b'', blank=True)),
                ('user', models.ForeignKey(related_name='reviewers', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Scout',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('address_text', models.CharField(max_length=500, null=True, blank=True)),
                ('city', models.CharField(max_length=50)),
                ('state', models.CharField(default=b'Telangana', max_length=30)),
                ('referer', models.CharField(max_length=50, null=True, blank=True)),
                ('additional_comments', models.TextField(default=b'', blank=True)),
                ('user', models.ForeignKey(related_name='scouts', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='record',
            name='created_by',
            field=models.ForeignKey(related_name='records', to='plant_images.Scout'),
        ),
        migrations.AddField(
            model_name='record',
            name='disease',
            field=models.ForeignKey(related_name='records', to='plant_images.Disease'),
        ),
        migrations.AddField(
            model_name='record',
            name='reviewed_by',
            field=models.ForeignKey(related_name='reviews', to='plant_images.Reviewer'),
        ),
    ]
