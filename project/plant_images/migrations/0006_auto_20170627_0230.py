# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('plant_images', '0005_auto_20170627_0224'),
    ]

    operations = [
        migrations.AlterField(
            model_name='record',
            name='gps',
            field=models.TextField(default=b'{}', null=True),
        ),
    ]
