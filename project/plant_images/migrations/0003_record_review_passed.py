# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('plant_images', '0002_auto_20170626_2017'),
    ]

    operations = [
        migrations.AddField(
            model_name='record',
            name='review_passed',
            field=models.BooleanField(default=False),
        ),
    ]
