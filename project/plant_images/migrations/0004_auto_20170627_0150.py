# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('plant_images', '0003_record_review_passed'),
    ]

    operations = [
        migrations.AlterField(
            model_name='record',
            name='reviewed_by',
            field=models.ForeignKey(related_name='reviews', blank=True, to='plant_images.Scout', null=True),
        ),
    ]
