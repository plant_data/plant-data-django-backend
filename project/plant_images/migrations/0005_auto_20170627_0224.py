# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('plant_images', '0004_auto_20170627_0150'),
    ]

    operations = [
        migrations.AddField(
            model_name='record',
            name='gps',
            field=models.TextField(default=b'{}', blank=True),
        ),
        migrations.AddField(
            model_name='record',
            name='photo_timestamp',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
    ]
