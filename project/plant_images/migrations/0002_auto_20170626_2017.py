# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('plant_images', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='reviewer',
            name='user',
        ),
        migrations.AddField(
            model_name='scout',
            name='can_review',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='record',
            name='reviewed_by',
            field=models.ForeignKey(related_name='reviews', to='plant_images.Scout'),
        ),
        migrations.DeleteModel(
            name='Reviewer',
        ),
    ]
