from rest_framework import permissions
from plant_images.models import Scout
from django.contrib.auth.models import User

class IsReviewer(permissions.BasePermission):

    def has_permission(self, request, view):
        scout = Scout.objects.filter(user=request.user).first()
        if scout:
            if(scout.can_review):
                return True

        return False
