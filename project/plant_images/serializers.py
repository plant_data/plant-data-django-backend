from rest_framework import serializers
from plant_images.models import Scout, Crop, Disease, Record
from django.contrib.auth.models import User

class CropSerializer(serializers.ModelSerializer):
    class Meta:
        model = Crop
        fields = ('id', 'created', 'name', 'crop_type', 'additional_comments', 'diseases')
        depth = 1

class DiseaseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Disease
        fields = ('id', 'created', 'name', 'crop', 'additional_comments')
        depth = 1

class RecordSerializer(serializers.ModelSerializer):
    class Meta:
        model = Record
        fields = "__all__"
        depth = 1

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'first_name', 'last_name', 'email')

class ScoutSerializer(serializers.ModelSerializer):
    user = UserSerializer(many=False)
    class Meta:
        model = Scout
        fields = ('created', 'user', 'address_text', 'city', 'state', 'can_review')
