from django.shortcuts import render
from rest_framework.decorators import permission_classes, parser_classes, api_view
from rest_framework.permissions import IsAuthenticated, AllowAny
from datetime import datetime
from rest_framework.views import APIView
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.response import Response
from rest_framework import generics
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from plant_images.models import Scout, Crop, Disease, Record
from plant_images.serializers import CropSerializer, DiseaseSerializer, RecordSerializer, ScoutSerializer
from django.core.files.base import ContentFile
from django.utils.crypto import get_random_string
from django.conf import settings
from rest_framework import status
from tempfile import NamedTemporaryFile
from django.db.models import Q
from django.contrib.auth.models import User
import urllib
from plant_images.permissions import IsReviewer
import json

# Create your views here.

class CropsList(generics.ListCreateAPIView):
    permission_classes=(IsAuthenticated, )
    authentication_classes = (JSONWebTokenAuthentication, )
    queryset = Crop.objects.all()
    serializer_class = CropSerializer

class CropDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes=(IsAuthenticated, )
    authentication_classes = (JSONWebTokenAuthentication, )
    queryset = Crop.objects.all()
    serializer_class = CropSerializer

class DiseasesList(generics.ListCreateAPIView):
    permission_classes=(IsAuthenticated, )
    authentication_classes = (JSONWebTokenAuthentication, )
    queryset = Disease.objects.all()
    serializer_class = DiseaseSerializer

'''
class MyUploads(APIView):
    permission_classes=(IsAuthenticated, )
    authentication_classes = (JSONWebTokenAuthentication, )

    def get(self, request):
        scout = Scout.objects.get(user=request.user)
        records = Record.objects.filter(created_by=scout)
        recordsSerialized = RecordSerializer(records, many=True)
        return Response(recordsSerialized.data)
'''

class MyUploads(generics.ListAPIView):
    permission_classes = (IsAuthenticated, )
    authentication_classes = (JSONWebTokenAuthentication, )
    serializer_class = RecordSerializer

    def get_queryset(self):
        scout = Scout.objects.filter(user=self.request.user).first()
        if scout:
            return Record.objects.filter(created_by=scout)
        else:
            return Record.objects.none()


class ToBeReviewed(generics.ListAPIView):
    permission_classes = (IsAuthenticated, IsReviewer)
    authentication_classes = (JSONWebTokenAuthentication, )
    serializer_class = RecordSerializer

    def get_queryset(self):
        user = Scout.objects.filter(user=self.request.user).first()
        if user:
            return Record.objects.filter( ~Q(created_by=user), is_reviewed=False )
        else:
            return Record.objects.none()

class ValidateRecord(APIView):
    permission_classes = (IsAuthenticated, IsReviewer)
    authentication_classes = (JSONWebTokenAuthentication, )

    def put(self, request):
        input_data = json.loads(request.body)
        record_id = input_data.get('record_id')
        validation = input_data.get('validation')
        print record_id
        print request.POST
        record = Record.objects.filter(pk=record_id).first()

        if (not record ):
            return Response({"error":"Record not found."}, status=404)

        if(record.is_reviewed):
            return Response({"error":"Record already reviewed."}, status=304)

        try:
            validation_int = int(validation)
        except Exception as e:
            print e
            return Response({"error":"Error reading validation data."}, status=400)

        if(validation_int):
            record.review_passed = True
        else:
            record.review_passed = False

        record.reviewed_by = Scout.objects.filter(user=request.user).first()
        record.is_reviewed = True

        try:
            record.save()

        except Exception as e:
            print e
            return Response({"error":"Unable to modify record in database"}, status=400)

        return Response(status=204)

class UserAccountExists(APIView):
    permission_classes = (AllowAny, )

    def get(self, request, format=None):
        queryEmailId =  urllib.unquote(request.GET.get('email', ''))
        account = User.objects.filter(email=queryEmail)
        if account:
            return Response({"result":True}, status=200)
        return Response({"result":False}, status=404)


class ScoutInfo(APIView):
    permission_classes=(IsAuthenticated, )
    authentication_classes = (JSONWebTokenAuthentication, )

    def get(self, request):
        scout = Scout.objects.filter(user=request.user).first()
        scoutSerialized = ScoutSerializer(scout)
        return Response(scoutSerialized.data)


class uploadRecord(APIView):
    permission_classes=(IsAuthenticated, )
    authentication_classes = (JSONWebTokenAuthentication, )

    def post(self, request, format=None):

        scout = Scout.objects.filter(user=request.user).first()
        if not scout:
            return Response({"error":"Unauthorized access"}, status=403)

        disease_id = request.POST.get('disease_id')
        crop_id = request.POST.get('crop_id')
        new_disease_name = request.POST.get('new_disease_name')
        gps = request.POST.get('gps')
        photo_timestamp = request.POST.get('photo_timestamp')
        upload = request.FILES['file']

        crop = Crop.objects.filter(pk=crop_id).first()
        if (not crop):
            return Response({"error":"Crop Record not found. Please contact Admin"}, status=404)

        if(not(disease_id) or disease_id == ""):#create new disease
            if not new_disease_name or new_disease_name == "":
                return Response({"error":"New disease name should not be null"}, status=400)
            existing_disease_with_same_name = Disease.objects.filter(Q(name__iexact=new_disease_name)).first()
            if(existing_disease_with_same_name):
                disease = existing_disease_with_same_name
            else:
                disease = Disease(name = new_disease_name, crop = crop)
                try:
                    disease.save()
                except Exception as e:
                    print e
                    return Response({"error":"Unable to save new disease in database"}, status=400)

        else:
            disease = Disease.objects.filter(pk=disease_id).first()
            if not disease:
                return Response({"error":"Unable to find the record for disease"}, status=404)

        #Saving the new record to db
        new_record = Record(created_by=scout, disease=disease, gps=gps, photo_timestamp=photo_timestamp)
        try:
            new_record.save()
        except Exception as e:
            print e
            return Response({"error":"Unable to save new record in database"}, status=400)

        if (not upload or not upload.name):
            return Response({'error':'Failed to read the upload file'}, status=400)

        extension =  upload.name.split(".")[-1]
        record_hash = get_random_string(8);
        file_name = "image_"+str(new_record.id)+"_"+record_hash+"."+extension

        try:
            temp_file = NamedTemporaryFile()
            original_path = temp_file.name

            for c in upload.chunks():
                temp_file.write(c)

            temp_file.seek(0)

            new_record.picture = ContentFile(temp_file.read(), file_name)
            new_record.save()
            temp_file.close()
            return Response({'validation':'OK', 'id':new_record.id, 'picture':file_name}, status=200)

        except Exception as e:
            print e
            return Response({"error":"file save failed"}, status=status.HTTP_507_INSUFFICIENT_STORAGE)
