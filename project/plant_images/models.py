from django.db import models
from datetime import datetime
from django.contrib.auth.models import User

# Create your models here.

BOOLEAN_CHOICES = (('yes', 'yes'), ('no', 'no'))

class Scout(models.Model):
    created = models.DateTimeField(auto_now_add = True)
    user = models.ForeignKey(User, related_name = "scouts")
    modified = models.DateTimeField(auto_now=True)
    address_text = models.CharField(max_length=500, null=True, blank=True)
    city = models.CharField(max_length=50)
    state = models.CharField(max_length=30, default="Telangana")
    referer = models.CharField(max_length=50, blank=True, null=True)
    can_review = models.BooleanField(default = False)
    additional_comments = models.TextField(blank=True, default="")

class Crop(models.Model):
    created = models.DateTimeField(auto_now_add = True)
    modified = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length=100)
    crop_type = models.CharField(max_length=50, blank=True, null=True)
    additional_comments = models.TextField(blank=True, default="")

class Disease(models.Model):
    created = models.DateTimeField(auto_now_add = True)
    modified = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length=100)
    crop = models.ForeignKey(Crop, related_name="diseases")
    additional_comments = models.TextField(blank=True, default="")


class Record(models.Model):
    created = models.DateTimeField(auto_now_add = True)
    modified = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(Scout, related_name="records", db_index=True)
    disease = models.ForeignKey(Disease, related_name="records")
    picture = models.ImageField(upload_to='disease_pictures', blank=True, null=True)
    gps = models.TextField(default="{}", null=True)
    photo_timestamp = models.CharField(max_length=100, blank=True, null=True)
    is_reviewed = models.BooleanField(default=False)
    reviewed_by = models.ForeignKey(Scout, related_name="reviews", blank=True, null=True)
    review_passed = models.BooleanField(default=False)
