from django.contrib import admin
from plant_images.models import Scout, Crop, Disease, Record


# Register your models here.
class CropAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Crop._meta.fields]
    model = Crop

class DiseaseAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Disease._meta.fields]
    model = Disease

class ScoutAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Scout._meta.fields]
    model = Scout

class RecordsAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Record._meta.fields]
    moderl = Record

admin.site.register(Crop, CropAdmin)
admin.site.register(Disease, DiseaseAdmin)
admin.site.register(Scout, ScoutAdmin)
admin.site.register(Record, RecordsAdmin)
admin.site.site_header = 'Plant Image Collection'
